
-- SUMMARY --

The Image progressive module allows to create images with interlaced bit. It 
means that image shows almost immediately and increase its quality during load 
process. Module works with gd image processing library and supports two file 
types: png, jpg.

To understand interlacing process look here:
  http://en.wikipedia.org/wiki/Interlacing_(bitmaps)

For a full description of the module, visit the project page:
  http://drupal.org/project/image_progressive

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/image_progressive


-- REQUIREMENTS --

None.


-- INSTALLATION --

* https://drupal.org/documentation/install/modules-themes/modules-7 for 
further information.


-- CONFIGURATION --

* Go to admin/config/media/image-styles andedit target style
* From dropdown list select "Progressive" option
* Add effect and that's it


-- CONTACT --

Author and maintainer:
* Tomasz Dobrzyński (depresz) - https://drupal.org/user/2409670
